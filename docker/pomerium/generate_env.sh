COOKIE_SECRET=$(head -c32 /dev/urandom |base64)
COOKIE_SECRET=$(string_replace_all "$COOKIE_SECRET" "/" "\\/")
saveEnvVar "COOKIE_SECRET"


AUTHENTICATE_SERVICE_URL=${HTTPS_ENDPOINT_AUTH_URL}
escapeUrl AUTHENTICATE_SERVICE_URL
saveEnvVar "AUTHENTICATE_SERVICE_URL"
unescapeVarString "AUTHENTICATE_SERVICE_URL"

APP_URL=${HTTPS_ENDPOINT_APP}
escapeUrl APP_URL
saveEnvVar "APP_URL"
unescapeVarString "APP_URL"

if [ "$STACK_ENV" == "dev" ]; then
	AUTOCERT=false
	saveEnvVar "AUTOCERT"

fi



# Copy templates
WCFS=${CONTAINER_DOCKER_DIR}/config_template_no_auth.yaml
WCFT=${CONTAINER_ENV_DIR}/config_template_no_auth.yaml
cp $WCFS $WCFT

WCFS=${CONTAINER_DOCKER_DIR}/config_template_with_auth.yaml
WCFT=${CONTAINER_ENV_DIR}/config_template_with_auth.yaml
cp $WCFS $WCFT
