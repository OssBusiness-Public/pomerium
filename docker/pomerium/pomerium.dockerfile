FROM pomerium/pomerium:v0.9.1 as base

ARG STACK_NAME
ARG STACK_VERSION
ARG STACK_ENV


ENV STACK_NAME=${STACK_NAME}
ENV STACK_VERSION=${STACK_VERSION}
ENV STACK_ENV=${STACK_ENV}

LABEL   "fi.oss-solutions.project"="$STACK_NAME" \
        "fi.oss-solutions.version"="$STACK_VERSION" \
        "fi.oss-solutions.env"="$STACK_ENV" \
        "fi.oss-solutions.service"="pomerium"


COPY pomerium/build_default_wildcard_cert.crt /pomerium/certs/cert.pem
COPY pomerium/build_default_wildcard_cert.key /pomerium/certs/certprivkey.pem


FROM base as prod


#CMD ["/root/boot_pomerium.sh"]

FROM base as dev

#CMD ["/root/boot_pomerium.sh"]