#!/bin/bash
source "$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )/../lib/bash_functions.sh"

containerDockerDirName "pomerium"
containerEnvDirName "pomerium"
importContainerEnvVars "pomerium"
containerLocalDirName "pomerium"
containerEnvFileName "pomerium"


if [ "$STACK_ENV" == "dev" ]; then
	WILDCARD_CERT="*.${STACK_BASE_URL}";
	makeCertForHost "${WILDCARD_CERT}" "${CONTAINER_ENV_DIR}" "wildcard_cert"
	if [ $EXIT_STATUS -eq  1 ]; then
		touch $DEPLOYMENT_STOP_LOCK_FILE
		exit;
	fi
fi

APP_URL=${HTTPS_ENDPOINT_APP}
escapeUrl "APP_URL"
saveEnvVar "APP_URL"
unescapeVarString "APP_URL"

if [ "$STACK_ENV" == "prod" ]; then
	if [ "$AUTOCERT" == false ]; then
		printNoticeBlock "No autocert in use"

		if [ ! -f "${CONTAINER_ENV_DIR}/docker-compose-local.yml" ]; then
			printErrorLine "Local certificates not defined, missing file 'docker-compose-local.yml'"
			cp "${PWD}/docker/pomerium/docker-compose-local.yml.template" "${PWD}/environment/pomerium/docker-compose-local.yml"
			printInfoLine "Template file copied to  'environment/pomerium/docker-compose-local.yml', open file for more instructions"
			touch $DEPLOYMENT_STOP_LOCK_FILE
			exit;
		else
			printInfoLine "No autocert in use, certificate management is manual. Look for 'environment/pomerium/docker-compose-local.yml' more info"

			ERRORS=0

			if [ ! -f "${CONTAINER_ENV_DIR}/wildcard_cert.crt" ]; then
				printErrorLine "Certificate file 'wildcard_cert.crt' missing"
				ERRORS=1
			fi

			if [ ! -f "${CONTAINER_ENV_DIR}/wildcard_cert.crt" ]; then
				printErrorLine "Certificate file 'wildcard_cert.crt' missing"
				ERRORS=1
			fi

			if [ $ERRORS -eq 1 ]; then
				printErrorBlock "Configuration seems to be incorrect"
				touch $DEPLOYMENT_STOP_LOCK_FILE
				exit;
			fi
		fi

	fi
fi


# Fix policy to match env settings
if [ "$SSL_ONLY" == true ]; then
	CONFIG_TEMPLATE_FILE="config_template_no_auth.yaml"
else
	CONFIG_TEMPLATE_FILE="config_template_with_auth.yaml"
fi

printInfoLine "Pomerium is using ${CONFIG_TEMPLATE_FILE} for policy"

CONFIG_TEMPLATE_FILE="${CONTAINER_ENV_DIR}/${CONFIG_TEMPLATE_FILE}"
CONFIG_DEPLOYMENT_FILE="$CONTAINER_ENV_DIR/config.yaml"

if [ -e $CONFIG_DEPLOYMENT_FILE ]; then
	rm -f $CONFIG_DEPLOYMENT_FILE
fi
cp "${CONFIG_TEMPLATE_FILE}" "${CONFIG_DEPLOYMENT_FILE}"

escapeUrl "APP_URL"
sed -i "s/APP_URL/${APP_URL}/" "${CONFIG_DEPLOYMENT_FILE}"
unescapeVarString "APP_URL"

if [[ "$SSL_ONLY" == "true" || "$STACK_ENV" == "dev" ]]; then
	
	IDP_PROVIDER="google"
	IDP_PROVIDER_URL="https://accounts.google.com"
	escapeUrl "IDP_PROVIDER_URL"
	IDP_SERVICE_ACCOUNT="ewogICJ0eXBlIjogInNlcnZpY2VfYWNjb3VudCIsCiAgInByb2plY3RfaWQiOiAibG9jYWxkZXYtMjY1OTEwIiwKICAicHJpdmF0ZV9rZXlfaWQiOiAiYnI5eWFhZ2dib3M4emFhdWZpYTJ3bzlhaXN2aWlsaTRqZXdvb2RveCIsCiAgInByaXZhdGVfa2V5IjogIi0tLS0tQkVHSU4gUFJJVkFURSBLRVktLS0tLVxuTUlJRXZBSUJBREFOQmdrcWhraUc5dzBCQVFFRkFBU0NCS1l3Z2dTaUFnRUFBb0lCQVFDOGxnT1FWTVl3SjBYK1xuejNyQmtXT1piMVlSZjFIUEFHRUtPVTVBOFlSQ0dLcXU5cFVqM1hqWi9UWmxoTzJ4V0JkUGVxL2VEdHlKNzFtYlxuNFg3WjlGd21wVW1lZjhuSkRRc3h3SDhQcXAra2IvWFA1SVFxemEvUThWNXR2ODdyZ2l5ZkhyT2o2RDJvbzkxZ1xuWkVERG1Cb0R0KzNCRExCVXViUzRNZjhRbVFjUjlhRGxsM0FQaHN1Q1F4T0JxYUF4ZXBnWFRKRzQ5b1NIS2l6ZFxucTZmQTEwVU9kMUZOQzhpd2pud0V2MXo3VlVxY1YxMldqZXhyUHc3L3E4T1VCZTVMY1lqSE4zV0hKU1crUFEzUVxuRDVFQzJEOXRDK3JYWFpGbWUyR3IycE96bE9tditmRmxWSjc3YTcyZFNMV1B5QVUzZEFSblRJb0tPYWhFZ2pvYVxuOGdnYnk0YXBBZ01CQUFFQ2dnRUFBMVFmaEhybWs1K0IxZ1ZKL0tRcERxSi9NYWhlWkltMGJNYUZxZEpKZkJRQ1xuNFJFem9USXR5YVUxUWxYMkR4RUh1a3luVVlOUUk3UWdrQUlHWitqdS9uU2c1OFlJSlA4UXNVUmRTY1RBZEJuYVxuekNaVHFlQy9CM0JYTXhjMVd3Vk5iZEtRSjZhMy9lUEVpbzRFc1o4YjZIQms1eWttN0p2cGcyM0llMzNEK081a1xuNFdMTThGQW53ZERPN3NMdlZUUG9scU95ZUllMWxnaVBEdGZlS0Q1NEEwR3l5Wm1QNWI0cFZVdy90QjF4cm9JdlxuaW41UzR4WkNaSlpkUmhMR0dHd01MdGdtVkxsQmx0MzByN2NkaWlWaDZrdFFqc1JRYndXem5OWFNQNVhwb3RsNlxuOVF6S0VuLzd4LzFyL3ppTGw1bHZlUWdMRkhmNmxWbnVKY3dJTlhIUVFRS0JnUUQ2dTZtZE5tNE1vclJ1TWhoZ1xucVVnQ0hFczd3S0QxNXdyazI5UjZoR1AxaXZmUm1iaktiVHFOaVZBcnp4REVHdzNUSnhVa1FLQkV2cmdnaTFLMVxuSHhISC9vL0hzY3VQWTJyL2s0aE9LK3NDbkpDNzlKK3N4RVFUcjFZbGdSZGwxRkJoNXcydnZaeHlHYTM4MEs3ZVxuZlgwRFpRVzdlUjFJdzFhVG5DWG03Z1dFWVFLQmdRREFqQ2FFaU5uZ3k1LzJycGdDVnhHYUFqN0h1bkF6QVQ2K1xuR3RIcnBmeStFRFJNa1RodnlHQ1ZzTTZocndkdkFGajNWYW53YU1haFdHaXZZc003SHl5cG9DTVRZeUtGeU50cFxudTVxZjVkTzMwczh6OVRHMFVFMHZOMWhRNVBEaEJ0am9naWFZcFRSUTR2L095bUF1Zit1WExNTzJLakJWc2FKZ1xuQTVwSFA4NG5TUUtCZ0Z3ZFI3SVBSaHowUlk5YWJpYUJPSWcxOXRuTDZoYU5QQzVhS05TZUFNODdyOUhjUk14ZlxubHBwYkFRNG5NNFNvWDhyY1IrTWFyd2F5ZnBqT05xTjk5NTFmQTFtZEV5anBvR3ltdWVQbU5KK3YvMmhkOWErRlxuSjJDSUFRRWtUUUZISUFUbnl0cHVzYVFsbFFzVmE3akptbno5eHo1WnY0MTAza0M2bEI0a0FmamhBb0dBTjVQOFxueGtMT1NUSGRrWkhoOU1MM0pMSkROdG1jNjB3UHRVWCtrL3VNa2J3UnJJdzZjeUd1cDJhcXJrYmNhWEQ3UFNGbFxuOVNINU83ZE9PRm5VcXFzYjlEWnlDWFdURUhUaXd3MVIyNWUyWGhndWtQdlVoZ3lyeXRZNTJ0ZktvOW1iSUxuNFxudEJUem5lbDRGMU9aSWJMdzZPa1Y4TzRLbDZHM2tuMGh1akV6cTJrQ2dZQkVyU2o1Nk1yRzlJN1dxT3VPOFdwMVxuaEIwWDY3S2pBSkFIN2pkckJKOUxEOHdCSEJTU1BJVTlHQ1FLcmhCY1EwNXFSOUhlUDA3NDkybXBRMldlWVV0RFxuZyt2eU5uSnl5VlhlV2FhN3JURW81cDlOY0d0bURKUVNsVGgxYlcxZExCRWNYN2V2UE5NamNOYVhNY3J3ZjRTNFxuTEwzN1VDUHRKMldCaEJTUllkOTNmcz09XG4tLS0tLUVORCBQUklWQVRFIEtFWS0tLS0tXG4iLAogICJjbGllbnRfZW1haWwiOiAicG9tZXJpdW1AbG9jYWxkZXYtMjY1OTEwLmlhbS5nc2VydmljZWFjY291bnQuY29tIiwKICAiY2xpZW50X2lkIjogIjY0OTgxODkzNjk3MzI3MjE2MTM4MSIsCiAgImF1dGhfdXJpIjogImh0dHBzOi8vYWNjb3VudHMuZ29vZ2xlLmNvbS9vL29hdXRoMi9hdXRoIiwKICAidG9rZW5fdXJpIjogImh0dHBzOi8vb2F1dGgyLmdvb2dsZWFwaXMuY29tL3Rva2VuIiwKICAiYXV0aF9wcm92aWRlcl94NTA5X2NlcnRfdXJsIjogImh0dHBzOi8vd3d3Lmdvb2dsZWFwaXMuY29tL29hdXRoMi92MS9jZXJ0cyIsCiAgImNsaWVudF94NTA5X2NlcnRfdXJsIjogImh0dHBzOi8vd3d3Lmdvb2dsZWFwaXMuY29tL3JvYm90L3YxL21ldGFkYXRhL3g1MDkvcG9tZXJpdW0lNDBsb2NhbGRldi0yNjU5MTAuaWFtLmdzZXJ2aWNlYWNjb3VudC5jb20iLAogICJpbXBlcnNvbmF0ZV91c2VyIjogInR1b21hcy5rb3BvbmVuQG9zcy1zb2x1dGlvbnMuZmkiCn0K"
	IDP_CLIENT_ID="none"
	IDP_CLIENT_SECRET="none"

	saveEnvVar "IDP_PROVIDER"
	saveEnvVar "IDP_PROVIDER_URL"
	saveEnvVar "IDP_SERVICE_ACCOUNT"
	saveEnvVar "IDP_CLIENT_ID"
	saveEnvVar "IDP_CLIENT_SECRET"
	unescapeVarString "IDP_PROVIDER_URL"

else 

	# Pack IDP_SERVICE_ACCOUNT config key
	if [ "$IDP_PROVIDER" == "google" ]; then
		if [[ "$IDP_SERVICE_ACCOUNT" == "" || "$IDP_SERVICE_ACCOUNT" == "json_file_base64" ]]; then

			FILE_NAME="google_auth.json"
			_FN="${CONTAINER_ENV_DIR}/${FILE_NAME}";
			if [ ! -e $_FN ]; then
				printErrorLine "Missing google public/private key pair json file ${_FN}"
				printInfoLine "Stored at the passvault search for IDP_SERVICE_ACCOUNT localdev.oss-solutions.fi"
				printInfoLine "You can save it under local/pomerium/${FILE_NAME}, it will be linked under environment folder and you can more easily regenerate required files"
				touch $DEPLOYMENT_STOP_LOCK_FILE
				exit
			fi
			
			IDP_SERVICE_ACCOUNT=$(cat ${_FN} |/bin/base64)
		fi
		
		# Busybox does word wrap without option to disable, remove that shit
		IDP_SERVICE_ACCOUNT=${IDP_SERVICE_ACCOUNT//[[:space:]]/}
		saveEnvVar "IDP_SERVICE_ACCOUNT"
	fi

	touch $DEPLOYMENT_STOP_LOCK_FILE
	if [ "$IDP_CLIENT_ID" == "fill-client-id-here" ] && [ "$STACK_ENV" == "prod" ]; then
		read -p "IDP_CLIENT_ID is not yet set [passvault localdev.oss-solutions.fi]: " IDP_CLIENT_ID
	fi
	unlink $DEPLOYMENT_STOP_LOCK_FILE
	saveEnvVar "IDP_CLIENT_ID"


	touch $DEPLOYMENT_STOP_LOCK_FILE
	if [ "$IDP_CLIENT_SECRET" == "fill-client-secret-here" ]  && [ "$STACK_ENV" == "prod" ]; then
		read -p "IDP_CLIENT_SECRET is not yet set [passvault localdev.oss-solutions.fi]: " IDP_CLIENT_SECRET
	fi
	unlink $DEPLOYMENT_STOP_LOCK_FILE
	saveEnvVar "IDP_CLIENT_SECRET"
fi
