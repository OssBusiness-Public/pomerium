#!/bin/bash 
source "$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )/bash_functions.sh"

registerContainerVars "pomerium"
importContainerEnvVars "pomerium"


DOMAIN=${STACK_BASE_URL}
TOKEN=${DNSIMPLE_API_TOKEN}

if [ "${TOKEN}" == "" ]; then
	printErrorBlock "DNSIMPLE_API_TOKEN missing, save it to pomerium's .env -file"
	touch $DEPLOYMENT_STOP_LOCK_FILE
	exit;
fi

DIR="${CONTAINER_ENV_DIR}/letsencrypt/"

CREDENTIAL_FILE="dnsimple_credentials.ini"
LOCAL_CREDENTIAL_FILE="${DIR}/$CREDENTIAL_FILE"
DOCKER_CREDENTIAL_FILE="/etc/letsencrypt/$CREDENTIAL_FILE"
CERTIFICATE_FILE="${DIR}/live/${DOMAIN}/fullchain.pem"
CERTIFICATE_KEY_FILE="${DIR}/live/${DOMAIN}/privkey.pem"

mkdir -p "${DIR}"

echo "dns_dnsimple_token = ${TOKEN}" > "${LOCAL_CREDENTIAL_FILE}"
chmod 0600 "${LOCAL_CREDENTIAL_FILE}"


docker run -it --rm --name certbot \
	-v "${DIR}:/etc/letsencrypt" \
	certbot/dns-dnsimple certonly \
		--no-eff-email \
		--cert-name "${DOMAIN}" \
		--email support@oss-solutions.fi \
		--agree-tos \
		--dns-dnsimple \
		--dns-dnsimple-credentials ${DOCKER_CREDENTIAL_FILE} \
		--domain ${DOMAIN}

rm "${LOCAL_CREDENTIAL_FILE}"


if [[ -f "${CERTIFICATE_FILE}" &&  -f "${CERTIFICATE_KEY_FILE}" ]]; then
	cat "${CERTIFICATE_FILE}" >  "${CONTAINER_ENV_DIR}/wildcard_cert.crt"
	cat "${CERTIFICATE_KEY_FILE}" >  "${CONTAINER_ENV_DIR}/wildcard_cert.key"
fi


