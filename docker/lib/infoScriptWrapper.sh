#!/bin/bash
source "$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )/bash_functions.sh"

OPTIND=1
SERVICES=""
while getopts "h?e:pds:" opt; do
    case "$opt" in
    h|\?)
        echo "

Docker container builder

-h  This help
-p  This does absolutely nothing
-s  Build only service (default all)
"
        exit 0
        ;;
    p)  # this exists cause we want to just pass forward all attributes from build
        ;;
    d)  # this exists cause we want to just pass forward all attributes from build
        ;;
    s)  SERVICES+=" $OPTARG"
        ;;
    e)  # not in use
        ;;
    esac
done

getAllContainersForEnv

for CONTAINER in "${CONTAINERS[@]}"; do
	if [[ "$SERVICES" == *"$CONTAINER"* || -z "$SERVICES" ]]; then
		containerInfoScriptFileName $CONTAINER
		if [ -e $CONTAINER_INFO_SCRIPT_FILE ]; then
			registerContainerVars $CONTAINER
			importContainerEnvVars $CONTAINER
			source $CONTAINER_INFO_SCRIPT_FILE
		fi
	fi
done