#!/bin/bash

export DSI_VERSION=1.2.0
export DBASH_VERSION=composer-php72v2
export STACK_ROOT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )/../../"
cd $STACK_ROOT_DIR;
STACK_ROOT_DIR=$PWD

STACK_ENV_DIR=${STACK_ROOT_DIR}/environment
STACK_DOCKER_DIR=${STACK_ROOT_DIR}/docker
STACK_DOCKER_LIB_DIR=${STACK_ROOT_DIR}/docker/lib
STACK_LOCAL_DIR=${STACK_ROOT_DIR}/local

REMOTE_SSH_KEY_DIR=~/.ssh
REMOTE_SSH_KEY_FILE="$REMOTE_SSH_KEY_DIR/id_rsa"
REMOTE_SSH_PUB_KEY_FILE="${REMOTE_SSH_KEY_FILE}.pub"
REMOTE_BUILD_TARGET=bob-the-builder.oss-solutions.fi
REMOTE_SSH_USER="dockeruser"
REMOTE_ROOT_DIR="~"
REMOTE_LOCK_FILE=".remote_lock_time"
REMOTE_LOCK_SOURCE_FILE=".remote_lock_source"

if [ "$DOCKER_GLOBAL_DEFAULTS_FILE" == "" ]; then
	DOCKER_GLOBAL_DEFAULTS_FILE="$HOME/.docker_global_environment"
fi

if [ ! -e $DOCKER_GLOBAL_DEFAULTS_FILE ]; then
	touch $DOCKER_GLOBAL_DEFAULTS_FILE
fi

if [ "$DOCKER_DEV_ROOT_CA_DIR" == "" ]; then
	DOCKER_DEV_ROOT_CA_DIR="$HOME/.docker_dev_root_ca"
fi

if [ ! -d $DOCKER_DEV_ROOT_CA_DIR ]; then
	mkdir -p "${DOCKER_DEV_ROOT_CA_DIR}"
fi


# Define that we can porceed. If we set this to true we will not deploy
export DEPLOYMENT_STOP_LOCK_FILE="${STACK_ENV_DIR}/.stop_deployment"

export BUILD_STOP_LOCK_FILE="${STACK_ENV_DIR}/.stop_build"


# CONTAINER_DOCKER_DIR = Contains all docker related config (docker/containername)
# CONTAINER_DIR = Container source code and run-in data (containername)
# CONTAINER_ENV_DIR = Container environmental variables and configs (environment/containername)
# CONTAINER_LOCAL_DIR = Container local configurations that needs to presists during environmental generation (local/containername)


for file in ${STACK_DOCKER_LIB_DIR}/modules/*.sh
do
  source ${file}
done

importStackEnv

if ! [ "$DBASH" == "ON" ]; then	# Validate required cmds only when not in dbash container

	if ! [ -x "$(command -v git)" ]; then
		printImportantBlock 'git command not found, can not proceed'
		exit 1
	fi

	if ! [ -x "$(command -v docker)" ]; then
		printImportantBlock 'docker not found, can not proceed'
		exit 1
	fi

	if ! [ -x "$(command -v docker-compose)" ]; then
		printImportantBlock 'docker-compose not found, can not proceed'
		exit 1
	fi

	if [ "$DOCKER_EXEC_AS_LOCAL_USER" == "false" ]; then
		DOCKER_EXEC_USER=""
	else
		DOCKER_EXEC_USER="--user $UID:$GID";
	fi
	
fi;


