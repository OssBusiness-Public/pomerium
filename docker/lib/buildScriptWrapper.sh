#!/bin/bash
source "$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )/bash_functions.sh"

OPTIND=1
SERVICES=""
while getopts "h?xbe:pds:" opt; do
    case "$opt" in
    h|\?)
        echo "

Docker container builder

-h  This help
-p  This does absolutely nothing
-e  set environment
-s  Build only service (default all)
"
        exit 0
        ;;
    p)  # this exists cause we want to just pass forward all attributes from build
        ;;
    d)  # this exists cause we want to just pass forward all attributes from build
        ;;
    s)  SERVICES+=" $OPTARG"
        ;;
    e)  STACK_ENV="${OPTARG}"
        ;;
    esac
done

getAllContainers

for CONTAINER in "${CONTAINERS[@]}"; do
	if [[ "$SERVICES" == *"$CONTAINER"* || -z "$SERVICES" ]]; then
		containerBuildScriptFileName $CONTAINER
		if [ -e $CONTAINER_BUILD_SCRIPT_FILE ]; then
			printNoticeBlock "Run build script for $CONTAINER"
			registerContainerVars $CONTAINER
			importContainerEnvVars $CONTAINER
			source $CONTAINER_BUILD_SCRIPT_FILE
		fi
	fi
done