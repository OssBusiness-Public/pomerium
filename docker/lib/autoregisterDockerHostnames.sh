#!/bin/bash
source "$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )/bash_functions.sh"

printHeaderBlock "Verify DNS names exists"

DOCKER_MACHINE_NAME=$(echo $DOCKER_MACHINE_NAME);
if [[ $DOCKER_MACHINE_NAME = "" ]]; then
	DOCKER_MACHINE_NAME='hyperkit'
	DOCKER_MACHINE_IP=127.0.0.1
else
	if ! [[ $DOCKER_MACHINE_IP =~ ^[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+$ ]]; then
		printImportantBlock "Could not find docker machine IP address";
		exit 1
	fi
fi

HOSTS_FILE="/etc/hosts";
HOSTS_FILE_OLD="/etc/hosts.old";

HOSTS_TMPFILE_NEW="/tmp/autoregisterDockerHosts.new"
DOCKER_CONTAINER_LIST="/tmp/autoregisterDockerHosts.containers"
DMPREFIX="docker-machine"


UPDATE_REQUIRED=false
declare -a REGISTER_HOSTNAMES;


REGISTER_HOSTNAMES+=($DOCKER_MACHINE_IP)
REGISTER_HOSTNAMES+=($DOCKER_MACHINE_NAME.$DMPREFIX)

# Locate docker container IP from /etc/hosts -file and verify does it has to be updated
if [ "$1" == "--regenerate" ]; then
	printInfoBlock "Cleaning up docker machines hostsnames";
	UPDATE_REQUIRED=true
else	
	AA=$(cat $HOSTS_FILE |grep $DOCKER_MACHINE_NAME.$DMPREFIX)
	IFS=' ' read -a REGISTERED_HOSTS <<< "${AA}"
	
	if ! [ "${#REGISTERED_HOSTS[@]}" -eq "0" ]; then
		for i in "${!REGISTERED_HOSTS[@]}"; do

			# IP is first, verify has it been changes
			if (($i == 0)); then
				if ! [ "$DOCKER_MACHINE_IP" == "${REGISTERED_HOSTS[$i]}" ]; then
					printInfoBlock "docker machines IP has changed, must regenerate";
					UPDATE_REQUIRED=true
				fi
			fi

			# first hostname must be dockermachineName.docker
			if (($i > 1)); then
				REGISTER_HOSTNAMES+=(${REGISTERED_HOSTS[$i]});
			fi
		done
	fi
fi

if [[ $AUTOREGISTER_HOSTNAMES_FROM_VARS = *[!\ ]* ]]; then
	IFS=',' read -r -a AUTOREGISTER_HOSTNAMES_FROM_VARS <<< "$AUTOREGISTER_HOSTNAMES_FROM_VARS"
	declare -a PUBLIC_DOCKER_HOSTNAMES
	for _AUTOREGISTER_HOSTNAME in "${AUTOREGISTER_HOSTNAMES_FROM_VARS[@]}"
	do
		IFS='.' read -r -a _VAR_ARRAY <<< "$_AUTOREGISTER_HOSTNAME"
		CONTAINER=${_VAR_ARRAY[0]}
		VAR_NAME=${_VAR_ARRAY[1]}
		importContainerEnvVars "$CONTAINER"
		_REGISTER_HOSTNAME=${!VAR_NAME}
		if [[ "$_REGISTER_HOSTNAME" =~ ^http.* ]]; then
			_REGISTER_HOSTNAME=$(echo $_REGISTER_HOSTNAME | awk -F[/:] '{print $4}')
		fi
		PUBLIC_DOCKER_HOSTNAMES+=($_REGISTER_HOSTNAME)
	done
else
	printNoticeBlock "AUTOREGISTER_HOSTNAMES_FROM_VARS not set, can not autoregister hostnames"
fi

for DOCKER_REGISTER_HOSTNAME in "${PUBLIC_DOCKER_HOSTNAMES[@]}"
do
  if [[ $DOCKER_REGISTER_HOSTNAME = *[!\ ]* ]]; then

	HOSTNAME_FOUND=false
	for i in "${REGISTER_HOSTNAMES[@]}"
	do
		if [ "$i" == "$DOCKER_REGISTER_HOSTNAME" ] ; then
			HOSTNAME_FOUND=true
		fi
	done

	if [ $HOSTNAME_FOUND = false ] ; then
		printInfoBlock "Found new host $DOCKER_REGISTER_HOSTNAME from $DOCKER_MACHINE_NAME with IP $DOCKER_MACHINE_IP"
		REGISTER_HOSTNAMES+=($DOCKER_REGISTER_HOSTNAME);

		UPDATE_REQUIRED=true
	fi
fi
done

if [ $UPDATE_REQUIRED = false ] ; then
	printInfoBlock "No changes, update not required"
	printInfoBlock "use --regenerate attribute to cleanup hosts-entry"
	exit 0
fi

printInfoBlock "Saving new hosts info"

NEW_HOSTS_ENTRY="${REGISTER_HOSTNAMES[*]}";

sed "/.*$DOCKER_MACHINE_NAME.*/d" $HOSTS_FILE > $HOSTS_TMPFILE_NEW
echo "$NEW_HOSTS_ENTRY" >> $HOSTS_TMPFILE_NEW

sudo mv $HOSTS_FILE $HOSTS_FILE_OLD
sudo mv $HOSTS_TMPFILE_NEW $HOSTS_FILE

printNoticeBlock "Regenerated $HOSTS_FILE, old versio stored to $HOSTS_FILE_OLD"