#!/bin/bash
source "$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )/bash_functions.sh"

if [ "$DBASH" != "ON" ]; then
    export DBASH="ON"
    dbash $0 "$@"
    exit;
fi

OPTIND=1
SERVICES=""
while getopts "h?e:ps:" opt; do
    case "$opt" in
    h|\?)
        echo "

Docker container Post script

-h  This help
-p  This does absolutely nothing
-e  set environment
-s  Run post script only service (default all)
"
        exit 0
        ;;
    p)  # this exists cause we want to just pass forward all attributes from build
        ;;
    d)  # this exists cause we want to just pass forward all attributes from build
        ;;
    s)  SERVICES+=" $OPTARG"
        ;;
    e)  STACK_ENV="${OPTARG}"
        ;;
    esac
done

getAllContainersForEnv

for CONTAINER in "${CONTAINERS[@]}"; do
	if [[ "$SERVICES" == *"$CONTAINER"* || -z "$SERVICES" ]]; then
		containerDeploymentPreScriptFileName $CONTAINER
		if [ -e $CONTAINER_DEPLOYMENT_PRE_SCRIPT_FILE ]; then
			registerContainerVars $CONTAINER
			importContainerEnvVars $CONTAINER
			source $CONTAINER_DEPLOYMENT_PRE_SCRIPT_FILE
		fi
	fi
done