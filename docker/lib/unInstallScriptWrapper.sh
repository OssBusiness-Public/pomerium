#!/bin/bash
source "$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )/bash_functions.sh"

OPTIND=1
SERVICES=""
while getopts "h?s:" opt; do
    case "$opt" in
    h|\?)
        echo "

Docker container uninstaller

-h  This help
-s  Build only service (default all)
"
        exit 0
        ;;
    s)  SERVICES+=" $OPTARG"
        ;;
    esac
done

getAllContainers

for CONTAINER in "${CONTAINERS[@]}"; do
	if [[ "$SERVICES" == *"$CONTAINER"* || -z "$SERVICES" ]]; then
		containerUninstallScriptFileName $CONTAINER
		if [ -e $CONTAINER_UNINSTALL_SCRIPT_FILE ]; then
			registerContainerVars $CONTAINER
			importContainerEnvVars $CONTAINER
			source $CONTAINER_UNINSTALL_SCRIPT_FILE
		fi
	fi
done