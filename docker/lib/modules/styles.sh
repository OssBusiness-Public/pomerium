function printHeaderBlock() {
	STRING_LEN=${#1}
	PAD_LEN=4
	HASH_LENGTH=$(($STRING_LEN + $PAD_LEN))

	PHASH="printf '%0.1s' "\\#"{1..$HASH_LENGTH}"
	printf "\033[1;33m"
	echo ""
	echo ""
	echo ""
	eval $PHASH
	echo ""
	echo "# $1 #"
	eval $PHASH
	echo ""
	printf "\033[0m\n";
}

function statusMessage() {
	printf "\r%s" "$1";
}

function printErrorLine() {
	printf "\033[1;91m"
	printf "~~~ $1 ~~~"
	printf "\033[0m\n";
}

function printImportantLine() {
	printf "\033[1;96m"
	printf "!!! $1 !!!"
	printf "\033[0m\n";
}

function printNoticeLine() {
	printf "\033[1;94m"
	printf "*** $1 ***"
	printf "\033[0m\n";
}

function printInfoLine() {
	printf "\033[1;36m"
	printf "* $1 *"
	printf "\033[0m\n";
}

function printLine() {
	printf "\033[1;32m"
	printf "$1"
	printf "\033[0m\n";
}





function printErrorBlock() {
	echo ""
	printErrorLine "$1"
	echo ""
}

function printImportantBlock() {
	echo ""
	printImportantLine "$1"
	echo ""
}

function printNoticeBlock() {
	echo ""
	printNoticeLine "$1"
	echo ""
}

function printInfoBlock() {
	echo ""
	printInfoLine "$1"
	echo ""
}
