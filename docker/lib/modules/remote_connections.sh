function ssh_remote() {
	ssh -o BatchMode=yes $REMOTE_SSH_USER@$REMOTE_BUILD_TARGET $@
}

function ssh_remote_tty() {
	ssh -t -o BatchMode=yes $REMOTE_SSH_USER@$REMOTE_BUILD_TARGET $@
}

function remote_locked() {
	REMOTE_LOCK_TIME=$(ssh_remote cat ${REMOTE_LOCK_FILE} 2>&1 |grep [0-9])
	if [ "$REMOTE_LOCK_TIME" == "" ]; then
		return 1
	fi

	_LOCAL_TIME=$(ssh_remote date -u +"%Y%m%d%H%M%S")
	((REMOTE_LOCK_DURATION=${REMOTE_LOCK_TIME}-${_LOCAL_TIME}))
	if [ "$REMOTE_LOCK_DURATION" -gt "0" ]; then
      return 0
    fi  

    return 1
	
}

function clear_my_remote_lock() {
	REMOTE_LOCK_SOURCE=$(ssh_remote cat ${REMOTE_LOCK_SOURCE_FILE} 2>&1)
	_local_uid
	if [ "$REMOTE_LOCK_SOURCE" == "$_LOCAL_UID" ]; then
		remote_lock_clear
	fi;

}

function wait_remote_to_unlock() {
	clear_my_remote_lock
	while remote_locked
	do
		statusMessage "Remote target is currently in use, lock will expire in ${REMOTE_LOCK_DURATION} seconds"
		sleep 9
	done
	echo ""
}

function remote_lock_clear() {
	ssh_remote "rm -f ${REMOTE_LOCK_FILE}; rm -f ${REMOTE_LOCK_SOURCE_FILE}"
	remote_cleanup_git_credentials
}


function set_remote_build_lock() {
	set_remote_lock "+10minutes"
}

function set_remote_push_lock() {
	set_remote_lock "+10minutes"
}

function set_remote_install_lock() {
	set_remote_lock "+20minutes"
}

function set_remote_uninstall_lock() {
	set_remote_lock "+6minutes"
}

function _local_uid() {
	_LOCAL_UID=$(md5 -q -s $(hostname)-${USER})
}

function set_remote_lock() {
	_LOCAL_TIME=$(ssh_remote date -u -d "$1" +"%Y%m%d%H%M%S")
	_local_uid
	ssh_remote "echo $_LOCAL_TIME > $REMOTE_LOCK_FILE; echo $_LOCAL_UID > $REMOTE_LOCK_SOURCE_FILE"

	remote_setup_git_credentials
}


function ssh_remote_has_access() {
	HAS_ACCESS=$(ssh -o BatchMode=yes $REMOTE_SSH_USER@$REMOTE_BUILD_TARGET $@ "echo 'ok'"  2>&1)
	if [[ "$HAS_ACCESS" == 'ok' ]]; then
	  return 0
	else 
	  return 1
	fi
}

function remoteStackRootDir() {
	REMOTE_STACK_ROOT_DIR="${REMOTE_ROOT_DIR}/${STACK_NAME}"
}

