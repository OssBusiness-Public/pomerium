function saveGlobalEnvVar() {
	printNoticeBlock "New global value stored $1 to ${DOCKER_GLOBAL_DEFAULTS_FILE}"
	saveEnvVar ${DOCKER_GLOBAL_DEFAULTS_FILE} "$1"
	
}

function envHasBeenSet() {
	if [ -e $STACK_ENV_DIR ]; then
		return 0;
	else
		return 1;
	fi
}

function exitIfEnvironmentIsNotSetUp() {
	if ! envHasBeenSet ; then
		printErrorBlock "Environment has not been set"
		printInfoLine "Run command 'bin/generate_environment_config'"
		exit
	fi
}


function saveEnvVar() {
	if [ "$#" -eq 2 ]; then
	    FILE=$1
		ENV_VAR=$2
	elif [ "$#" -eq 1 ]; then
		FILE=$CONTAINER_ENV_FILE
		ENV_VAR=$1
	else
		 echo "Illegal number of parameters"
		 exit 1
	fi
	
	ENV_VAL=${!ENV_VAR}
	
	if [ ! -e $FILE ]; then
		touch $FILE
	fi

	VAR_EXISTS=$(cat $FILE | grep -c "$ENV_VAR=")
	if [ $VAR_EXISTS -eq 0 ]; then
	   printf "$ENV_VAR=\n" >> $FILE	
	fi

	# I do not want to make backups and we might have permissions for this file only (not for tmp)
	_OUTPUT=$(sed "s/^$ENV_VAR=.*/$ENV_VAR=$ENV_VAL/" $FILE)
	echo "$_OUTPUT" > $FILE
}


function importStackEnv() {
	if [ -e $DOCKER_GLOBAL_DEFAULTS_FILE ]; then
		importEnv $DOCKER_GLOBAL_DEFAULTS_FILE
	fi

	if [ -e $STACK_DOCKER_DIR/.stack_env ]; then
		importEnv $STACK_DOCKER_DIR/.stack_env
	fi
	
	# some variable needs to be sticky when running wrapper scripts
	if [ "$STACK_ENV" != "" ]; then
		STICKY_STACK_ENV=$STACK_ENV
	fi

	if [ -e $STACK_ENV_DIR/.env ]; then
		importEnv $STACK_ENV_DIR/.env
	fi

	if [ "$STICKY_STACK_ENV" != "" ]; then
		STACK_ENV=$STICKY_STACK_ENV
	fi
}

function noticeProdEnv() {
	if [ $STACK_ENV == "prod" ]; then
		printImportantBlock "You are in $STACK_ENV environment"

		typeset -i COUNTER_DELAY_SEC
		let COUNTER_DELAY_SEC=1
		while ((COUNTER_DELAY_SEC>0)); do

			printf '\r %2d\r' $COUNTER_DELAY_SEC
		    let COUNTER_DELAY_SEC--
		    sleep 1
		done
		printf '\r Proceed... \r' $COUNTER_DELAY_SEC
		echo ""
		echo ""
	fi
}


function containerEnvFileExists() {
	containerEnvFileName $1
	if [ -e $CONTAINER_ENV_FILE ]; then
		return 0;
	else
		return 1;
	fi
}

function generateEnvFile() {
	CONTAINER_NAME=$1

	containerEnvFileExists $CONTAINER_NAME
	containerEnvTemplateFileName $CONTAINER_NAME
	containerEnvGeneratorFileName $CONTAINER_NAME
	if ! containerEnvFileExists $CONTAINER_NAME; then
		containerEnvTemplateFileName $CONTAINER_NAME
		containerEnvGeneratorFileName $CONTAINER_NAME
		containerEnvFileName $CONTAINER_NAME

		if [ "$QUIET" == false ]; then
			echo ""
			printHeaderBlock "Generate valid configuration for ${STACK_NAME} ${CONTAINER_NAME}"
			echo ""
		fi
		mkdir -p $CONTAINER_ENV_DIR
		cp $CONTAINER_ENV_TEMPLATE_FILE $CONTAINER_ENV_FILE

	    source $CONTAINER_ENV_GENERATOR_FILE

	    printNoticeBlock "Generated enviroment config for ${STACK_NAME} ${CONTAINER_NAME}"

	else

		printInfoBlock "Environment config exists for ${STACK_NAME} ${CONTAINER_NAME}, updating.."
		importContainerEnvVars $CONTAINER_NAME
		source $CONTAINER_ENV_GENERATOR_FILE
		
	fi
}

