function debug() {
	local _VAR=$1
	local _VAL=${!_VAR}
	echo "$_VAR: '${_VAL}'"
}

function trimr() {
	VAR_NAME=$1
	TEMPVAL=${!VAR_NAME}
	TEMPVAL=${TEMPVAL//$'\n'/}
	TEMPVAL=${TEMPVAL//$'\r'/}
	
	if [ "${DBASH}" == "ON" ]; then
		declare -n ref="${VAR_NAME}"
		ref=$TEMPVAL
	else 
		eval "$VAR_NAME"="$TEMPVAL"
	fi
}


function containerBuildTime() {
	if [ "$STACK_ENV" == "dev" ]; then
		_TAG="dev"
	else
		_TAG=$STACK_VERSION
	fi

	CB=$(docker images \
	    --filter "label=fi.oss-solutions.project=${STACK_NAME}" \
	    --filter "label=fi.oss-solutions.env=${STACK_ENV}" \
	    --filter "label=fi.oss-solutions.service=${CONTAINER}" \
	    --filter "label=fi.oss-solutions.version=${STACK_VERSION}" \
	    ${DOCKER_REGISTRY_URL}/${STACK_NAME}/${CONTAINER}:${_TAG} \
	    --format "{{.ID}}")

	if [ "$CB" == "" ]; then
		CONTAINER_BUILD_TIME="";
		return 1
	fi

	CB=$(docker inspect $CB -f {{.Metadata.LastTagTime}})

	VAR_LEN=${#CB}
	if [ "$VAR_LEN" -lt "35" ]; then
		CONTAINER_BUILD_TIME="";
		return 2
	fi

	CONTAINER_BUILD_TIME=$(dbash "echo '${CB}' | sed -r 's/^([0-9]{4})-([0-9]{2})-([0-9]{2}) ([0-9]{2}):([0-9]{2}):([0-9]{2}).[0-9]{5,9} (.[0-9]{4}).*$/\1-\2-\3 \4:\5:\6 \7/g'")
	# Cleanup trailing newline
	CONTAINER_BUILD_TIME=${CONTAINER_BUILD_TIME//$'\n'/}
	
}


function hardlinkLocalData {
	getAllContainers
	for CONTAINER in "${CONTAINERS[@]}"; do
		hardlinkContainerLocalData $CONTAINER
	done
}


function hardlinkContainerLocalData {
	CONTAINER=$1
	containerLocalDirName "$CONTAINER"
	containerEnvDirName "$CONTAINER"
	for F in $CONTAINER_LOCAL_DIR/*; do
		if [ -f "${F}" ]; then

			local FN=$(basename $F)
			local TARGET_FILENAME="${CONTAINER_ENV_DIR}/${FN}"
			if [ -e "${TARGET_FILENAME}" ]; then
				rm -f "${TARGET_FILENAME}"
			fi
			sleep 1
			ln "${F}" "${TARGET_FILENAME}"
		fi
	done
}

function string_replace_all {
	string=$1
	echo "${string//$2/$3}"
}

function importEnv() {
	ENV_FILE=$1
	IMPORT_VAR=$2
	if [[ ! -z "$ENV_FILE" ]] ; then
		if [ -f "$ENV_FILE" ]; then
			while read LINE; do
				IFS='=' read -r -a VARS <<< "$LINE"
				ASSIGN_TO_VAR=${VARS[0]}
				VAR_VALUE=${VARS[1]}
				if [[ ! "$ASSIGN_TO_VAR" =~ ^\s*[#].*$ ]]; then 
					if [[ "$ASSIGN_TO_VAR" == "$IMPORT_VAR" || "$IMPORT_VAR" == "" ]]; then
						if [[ "$ASSIGN_TO_VAR" != "" && "$VAR_VALUE" != "" ]]; then
							eval "export $ASSIGN_TO_VAR='$VAR_VALUE'"
						fi
					fi
				fi
			done < $ENV_FILE
		fi
		return 0;
	fi
}

function escapeUrl() {
	VAR_NAME=$1
	TEMPVAL=${!VAR_NAME}
	TEMPVAL=$(string_replace_all "$TEMPVAL" ":" "\\:")
	TEMPVAL=$(string_replace_all "$TEMPVAL" "/" "\\/")
	
	declare -n ref="${VAR_NAME}"
	ref=$TEMPVAL
}
	
function unescapeVarString() {
	VAR_NAME=$1
	TEMPVAL=${!VAR_NAME}
	TEMPVAL=$(string_replace_all "$TEMPVAL" "\\\\" "")
	TEMPVAL=$(string_replace_all "$TEMPVAL" "\\" "")

	declare -n ref="${VAR_NAME}"
	ref=$TEMPVAL
}

function getUrlHost() {
	URL=$1
	URL_HOST=$(echo "${URL}" | sed -e 's|^[^/]*//||' -e 's|/.*$||')
}

function makeCertForHost() {
	HOST=$1
	DIR=$2
	FILENAME=$3
	${STACK_DOCKER_LIB_DIR}/makeSelfSignedSSL.sh -n "${HOST}" -d ${DIR} -f ${FILENAME}  >/dev/null 2>&1
	EXIT_STATUS=$?

	if [ $EXIT_STATUS -eq  1 ]; then
		printErrorBlock "Development Root CA is missing"
		printInfoLine "Generate new key running command 'docker/lib/makeSelfSignedSSL.sh -r'"
	fi
	
}


function ifEmpty() {
	VAR_NAME=$1
	VAR_VAL=${!VAR_NAME}
	DEFAULT_VALUE=$2
	if [[ $VAR_VAL != *[!\ ]* ]]; then
		declare -n ref="${VAR_NAME}"
		ref=$DEFAULT_VALUE
	fi
}


function dbash () {
	CMD="export DBASH="ON"; export DOCKER_GLOBAL_DEFAULTS_FILE=$DOCKER_GLOBAL_DEFAULTS_FILE; export DOCKER_DEV_ROOT_CA_DIR=$DOCKER_DEV_ROOT_CA_DIR; "
	CMD+="$@"
	docker run --rm --user ${UID}:${GID} -v ${STACK_ROOT_DIR}:${STACK_ROOT_DIR}:consistent -v ${DOCKER_DEV_ROOT_CA_DIR}:${DOCKER_DEV_ROOT_CA_DIR}:consistent -v ${DOCKER_GLOBAL_DEFAULTS_FILE}:${DOCKER_GLOBAL_DEFAULTS_FILE}:consistent -w ${STACK_ROOT_DIR} -i -t registry.gitlab.com/ossbusiness-public/docker-stack-installer/dbash:$DBASH_VERSION bash -c "$CMD"
}
