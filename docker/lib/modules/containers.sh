function containerDockerFile() {
	CONTAINER_NAME=$1
	containerDockerDirName $CONTAINER_NAME
	CONTAINER_DOCKER_FILE="${CONTAINER_DOCKER_DIR}"/$CONTAINER_NAME.dockerfile
}

function containerEnvFileName() {
	CONTAINER_NAME=$1
	containerEnvDirName $CONTAINER_NAME
	CONTAINER_ENV_FILE="${CONTAINER_ENV_DIR}"/.env
}

function containerEnvTemplateFileName() {
	CONTAINER_NAME=$1
	containerEnvDirName $CONTAINER_NAME
	CONTAINER_ENV_TEMPLATE_FILE="${CONTAINER_DOCKER_DIR}"/.env.template
}

function containerEnvGeneratorFileName() {
	CONTAINER_NAME=$1
	containerDockerDirName $CONTAINER_NAME
	CONTAINER_ENV_GENERATOR_FILE="${CONTAINER_DOCKER_DIR}"/generate_env.sh
}

function containerInfoScriptFileName() {
	CONTAINER_NAME=$1
	containerDockerDirName $CONTAINER_NAME
	CONTAINER_INFO_SCRIPT_FILE="${CONTAINER_DOCKER_DIR}"/info.sh
}

function containerBuildScriptFileName() {
	CONTAINER_NAME=$1
	containerDockerDirName $CONTAINER_NAME
	CONTAINER_BUILD_SCRIPT_FILE="${CONTAINER_DOCKER_DIR}"/build.sh
}

function containerInstallScriptFileName() {
	CONTAINER_NAME=$1
	containerDockerDirName $CONTAINER_NAME
	CONTAINER_INSTALL_SCRIPT_FILE="${CONTAINER_DOCKER_DIR}"/install.sh
}

function containerUninstallScriptFileName() {
	CONTAINER_NAME=$1
	containerDockerDirName $CONTAINER_NAME
	CONTAINER_UNINSTALL_SCRIPT_FILE="${CONTAINER_DOCKER_DIR}"/uninstall.sh
}

function containerInitScriptFileName() {
	CONTAINER_NAME=$1
	containerDockerDirName $CONTAINER_NAME
	CONTAINER_INIT_SCRIPT_FILE="${CONTAINER_DOCKER_DIR}"/init.sh
}

function containerDeploymentPreScriptFileName() {
	CONTAINER_NAME=$1
	containerDockerDirName $CONTAINER_NAME
	CONTAINER_DEPLOYMENT_PRE_SCRIPT_FILE="${CONTAINER_DOCKER_DIR}"/deploymentPre.sh
}

function containerDeploymentPostScriptFileName() {
	CONTAINER_NAME=$1
	containerDockerDirName $CONTAINER_NAME
	CONTAINER_DEPLOYMENT_POST_SCRIPT_FILE="${CONTAINER_DOCKER_DIR}"/deploymentPost.sh
}

function mergedDockerComposeFileAttributes() {
	WITH_BUILD=$1
	shift 1
	ONLY_SERVICES=" $@ "
	
	DOCKER_MERGED_COMPOSE_FILES=""

	if [ -e docker/docker-compose-$STACK_ENV.yml ]; then
	    DOCKER_MERGED_COMPOSE_FILES="$DOCKER_MERGED_COMPOSE_FILES --compose-file docker/docker-compose-$STACK_ENV.yml"
	fi;

	if [ -e environment/docker-compose-local.yml ]; then
		printNoticeLine "Local config 'environment/docker-compose-local.yml' applied"
	   	DOCKER_MERGED_COMPOSE_FILES="$DOCKER_MERGED_COMPOSE_FILES --compose-file environment/docker-compose-local.yml"
	fi;

	if [ "$WITH_BUILD" == "BUILD" ]; then
		if [ -e docker/docker-compose-build.yml ]; then
			DOCKER_MERGED_COMPOSE_FILES="$DOCKER_MERGED_COMPOSE_FILES --compose-file docker/docker-compose-build.yml"
		fi
		
		if [ -e docker/docker-compose-$STACK_ENV-build.yml ]; then
		    DOCKER_MERGED_COMPOSE_FILES="$DOCKER_MERGED_COMPOSE_FILES --compose-file docker/docker-compose-$STACK_ENV-build.yml"
		fi;

	fi 

	getAllContainersForEnv
	for CONTAINER in "${CONTAINERS[@]}"; do
		local APPEND=true;
		if [ "$ONLY_SERVICES" != "  " ]; then
			if [[ $ONLY_SERVICES != *" ${CONTAINER} "* ]]; then
				APPEND=false
			fi
		fi
		
		if $APPEND; then
			if [ -e docker/$CONTAINER/docker-compose.yml ]; then
				DOCKER_MERGED_COMPOSE_FILES="$DOCKER_MERGED_COMPOSE_FILES --compose-file docker/$CONTAINER/docker-compose.yml"
			fi

		    if [ -e docker/$CONTAINER/docker-compose-$STACK_ENV.yml ]; then
			    DOCKER_MERGED_COMPOSE_FILES="$DOCKER_MERGED_COMPOSE_FILES --compose-file docker/$CONTAINER/docker-compose-$STACK_ENV.yml"
			fi;

			if [ -e environment/$CONTAINER/docker-compose-local.yml ]; then
				printNoticeLine "Local config 'environment/$CONTAINER/docker-compose-local.yml' applied"
			   	DOCKER_MERGED_COMPOSE_FILES="$DOCKER_MERGED_COMPOSE_FILES --compose-file environment/$CONTAINER/docker-compose-local.yml"
			fi;

			if [ "$WITH_BUILD" == "BUILD" ]; then
				if [ -e docker/$CONTAINER/docker-compose-build.yml ]; then
					DOCKER_MERGED_COMPOSE_FILES="$DOCKER_MERGED_COMPOSE_FILES --compose-file docker/$CONTAINER/docker-compose-build.yml"
				fi

				if [ -e docker/$CONTAINER/docker-compose-$STACK_ENV-build.yml ]; then
				    DOCKER_MERGED_COMPOSE_FILES="$DOCKER_MERGED_COMPOSE_FILES --compose-file docker/$CONTAINER/docker-compose-$STACK_ENV-build.yml"
				fi;

			fi 
		fi
	done

	DOCKER_MERGED_COMPOSE_FILES="$DOCKER_MERGED_COMPOSE_FILES --compose-file docker/docker-compose.yml"


}

function mergeDockerFileAttributes() {

	WITH_BUILD=$1
	shift 1

	mergedDockerComposeFileAttributes $WITH_BUILD "$@"
	DOCKER_MERGED_FILES=${DOCKER_MERGED_COMPOSE_FILES//compose-file/file}

}

function containerEnvDirName() {
	CONTAINER_NAME=$1
	if [ "$CONTAINER_NAME" == "stack" ]; then
		CONTAINER_ENV_DIR=$STACK_ENV_DIR
	else 
		CONTAINER_ENV_DIR="${STACK_ENV_DIR}/$CONTAINER_NAME"
	fi
}

function containerLocalDirName() {
	CONTAINER_NAME=$1
	if [ "$CONTAINER_NAME" == "stack" ]; then
		CONTAINER_LOCAL_DIR=$STACK_LOCAL_DIR/
	else 
		CONTAINER_LOCAL_DIR="${STACK_LOCAL_DIR}/$CONTAINER_NAME"
	fi
}

function containerDockerDirName() {
	CONTAINER_NAME=$1
	if [ "$CONTAINER_NAME" == "stack" ]; then
		CONTAINER_DOCKER_DIR=$STACK_DOCKER_DIR
	else 
		CONTAINER_DOCKER_DIR="${STACK_DOCKER_DIR}/$CONTAINER_NAME"
	fi
}

function containerDirName() {
	CONTAINER_NAME=$1
	if [ "$CONTAINER_NAME" == "stack" ]; then
		CONTAINER_DIR=$STACK_ROOT_DIR
	else 
		CONTAINER_DIR="${STACK_ROOT_DIR}/$CONTAINER_NAME"
		mkdir -p $CONTAINER_DIR
	fi
}


function getAllContainers() {
	IFS=',' read -r -a CONTAINERS <<< "$ALL_CONTAINERS"
}

function getDevContainers() {
	IFS=',' read -r -a CONTAINERS <<< "$DEV_CONTAINERS"
}

function getProdContainers() {
	IFS=',' read -r -a CONTAINERS <<< "$PROD_CONTAINERS"
}

function getBuildContainers() {
	IFS=',' read -r -a CONTAINERS <<< "$BUILD_CONTAINERS"
}

function getAllContainersForEnv() {
	CONTAINERS_FOR_ENV=()
	CONTAINERS=()
	if [ "$STACK_ENV" == "prod" ]; then
		getDevContainers
	elif [ "$STACK_ENV" == "dev" ]; then
		getProdContainers
	elif [ "$STACK_ENV" == "build" ]; then
		getBuildContainers
	else
		printErrorBlock "Unknown env ${STACK_ENV}"
		exit;
	fi
	CONTAINERS_FOR_ENV=${CONTAINERS}
}

function registerContainerVars() {
	CONTAINER=$1
	containerDockerDirName $CONTAINER
	containerDirName $CONTAINER
	containerDockerFile $CONTAINER
	containerEnvDirName $CONTAINER
	containerEnvTemplateFileName $CONTAINER
	containerEnvGeneratorFileName $CONTAINER
	containerDeploymentPreScriptFileName $CONTAINER
	containerDeploymentPostScriptFileName $CONTAINER
	containerEnvFileName $CONTAINER
}

function importContainerEnvVars() {
	CONTAINER=$1
	containerEnvFileName $CONTAINER
	importEnv $CONTAINER_ENV_FILE
}

function importContainerEnvVar() {
	_C=$CONTAINER
	CONTAINER=$1
	IMPORT_VAR=$2
	containerEnvFileName $CONTAINER
	importEnv $CONTAINER_ENV_FILE $IMPORT_VAR
	CONTAINER=$_C
	registerContainerVars $CONTAINER
}


function containerIsRunning() {
	local TEST

	#	Can not get data out of docker insie dbash
	if [ "$DBASH" == "ON" ]; then
		printErrorLine "Inside DBASH we can not detect container status"
	fi

	CONTAINER_NAME=$1
	TEST=$(docker ps |grep ${STACK_NAME}_${CONTAINER_NAME})
	if [ "$TEST" == "" ]; then
		return 1
	else
		return 0
	fi
}
