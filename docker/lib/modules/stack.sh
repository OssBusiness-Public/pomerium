function stackStatus() {
	local STACK_IS_RUNNING=false
	local CONTAINERS_RUNNING_COUNT=0
	local CONTAINERS_COUNT=0

	#	Can not get data out of docker insie dbash
	if [ "$DBASH" == "ON" ]; then
		printErrorLine "Inside DBASH we can not detect container status"
	fi


	getAllContainersForEnv
	for CONTAINER in "${CONTAINERS_FOR_ENV[@]}"; do
		if containerIsRunning $CONTAINER ; then
			CONTAINERS_RUNNING_COUNT=$((CONTAINERS_RUNNING_COUNT + 1))
			CONTAINER_RUNNING=$CONTAINER
			#debug "CONTAINER_RUNNING"
		else
			CONTAINER_NOT_RUNNING=$CONTAINER
			#debug "CONTAINER_NOT_RUNNING"
		fi
		CONTAINERS_COUNT=$((CONTAINERS_COUNT + 1))
	done


	if [ $CONTAINERS_COUNT -eq $CONTAINERS_RUNNING_COUNT ] ; then
		STACK_STATUS="running"
	elif [ $CONTAINERS_RUNNING_COUNT -eq 0 ]; then
		STACK_STATUS="down"
	else
		STACK_STATUS="dangling"
	fi
}

function stackIsRunning() {
	stackStatus
	if [ "$STACK_STATUS" == "running" ] ; then
		return 0
	else
		return 1
	fi
}

function stackIsNotRunning() {
	stackStatus
	if [ "$STACK_STATUS" == "down" ] ; then
		return 0
	else
		return 1
	fi
}

function runInfoScripts() {
	bash ${STACK_DOCKER_LIB_DIR}/infoScriptWrapper.sh "$@"
}

function runBuildScripts() {
	bash ${STACK_DOCKER_LIB_DIR}/buildScriptWrapper.sh "$@"
}

function runInstallScripts() {
	bash ${STACK_DOCKER_LIB_DIR}/installScriptWrapper.sh
}

function runUninstallScripts() {
	bash ${STACK_DOCKER_LIB_DIR}/unInstallScriptWrapper.sh
}

function runInitScripts() {
	bash ${STACK_DOCKER_LIB_DIR}/initScriptWrapper.sh
}

function runDeploymentPreScripts() {
	bash ${STACK_DOCKER_LIB_DIR}/deploymenPreScriptWrapper.sh
}

function runDeploymentPostScripts() {
	bash ${STACK_DOCKER_LIB_DIR}/deploymenPostScriptWrapper.sh
}

