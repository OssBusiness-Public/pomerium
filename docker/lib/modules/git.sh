function gitRemoteUrl() {
	GIT_REMOTE_URL="$($STACK_DOCKER_LIB_DIR/gitSubmoduleAuthUrls.sh $(git config --get remote.origin.url))"
}

function gitSubmoduleRemoteUrls() {
	GIT_SUBMODULE_REMOTE_URLS=$(git submodule foreach -q $STACK_DOCKER_LIB_DIR/gitSubmoduleAuthUrls.sh $(git config remote.origin.url);)
}


function exitIfGitHasPendingChanges() {
	GIT_STATUS=$(git status -s)
	if [ -n "$GIT_STATUS" ]; then
		printImportantBlock "Git has pending changes, can not proceed"
		git status -s
		echo ""
		echo ""
		exit 1
	fi

}

function remote_setup_git_credentials() {
	gitSubmoduleRemoteUrls

	ssh_remote "git config --global credential.helper store; echo $GIT_SUBMODULE_REMOTE_URLS > ~/.git-credentials"
	ssh_remote "echo $GIT_ACCESS_TOKEN | docker login --password-stdin --username $GIT_USERNAME ${DOCKER_REGISTRY} 2>/dev/null"

	USERNAME=$(git config user.name)
	USEREMAIL=$(git config user.email)

	if [ "${USERNAME}${USEREMAIL}" == "" ]; then
		printErrorLine "Please set git username and email to your git client (Tower)"
		exit;
	fi 

	PUSH_DEFAULT=$(git config push.default)
	ssh_remote "git config --global user.name '$USERNAME'; git config --global user.email '$USEREMAIL'; git config --global push.default '$PUSH_DEFAULT'"
	
}

function remote_cleanup_git_credentials() {
	ssh_remote "rm -rf ~/.git-credentials; docker logout ${DOCKER_REGISTRY}"
}
