#!/bin/bash
source "$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )/bash_functions.sh"

OPTIND=1
DNS=""
CREATE_ROOT_CA="false"
while getopts "h?rd:n:f:" opt; do
    case "$opt" in
    h|\?)
        echo "

Docker container builder

-h  This help
-n  DNS name
-d  Dirname for certs
-f  Filename base for certs, don't include extension
-r 	Create new Root CA
"
        exit 0
        ;;
    n)  DNS="$OPTARG"
        ;;
    d)  CERT_DIR="$OPTARG"
        ;;
    f)  CERT_FILENAME="$OPTARG"
        ;;
    r)  CREATE_ROOT_CA="true"
        ;;
    esac
done

ROOT_CA_KEY="${DOCKER_DEV_ROOT_CA_DIR}/local_dev_root-ca.key"
ROOT_CA_PEM="${DOCKER_DEV_ROOT_CA_DIR}/local_dev_root-ca.pem"
CA_SERIAL="${DOCKER_DEV_ROOT_CA_DIR}/local_dev_root-ca.file.srl"

PASS="OSS_LOCAL_DEV_CA_PASS_er5myape"
CERT_SUBJ="/CN=OSS Local Dev Cert Authority/OU=Development/O=OSS Business Solutions Oy/L=Lahti/ST=Lahti/C=FI"

mkdir -p "${DOCKER_DEV_ROOT_CA_DIR}"

if [ "${CREATE_ROOT_CA}" == "true" ]; then
	printImportantLine "Generating new Development Root CA, your local password will be required"

	openssl genrsa -des3 -passout pass:${PASS} -out ${ROOT_CA_KEY} 4096 
	openssl req -x509 -new -passin pass:${PASS} -subj "${CERT_SUBJ}" -nodes -key ${ROOT_CA_KEY} -sha256 -days 1825 -out ${ROOT_CA_PEM} 
	sudo security add-trusted-cert -d -r trustRoot -k "/Library/Keychains/System.keychain" ${ROOT_CA_PEM}
	
	printInfoLine "New Root CA installed"

	exit
fi



if [ "$DNS" == "" ]; then
	printErrorLine "DNS required when creating site sertificate"
fi

if [ "$CERT_DIR" == "" ]; then
	printErrorLine "Dirname required when creating site sertificate"
	exit
fi

ifEmpty "CERT_FILENAME" "$DNS"


SITE_CNF="${CERT_DIR}/${CERT_FILENAME}.cnf"
SITE_KEY="${CERT_DIR}/${CERT_FILENAME}.key"
SITE_CRT="${CERT_DIR}/${CERT_FILENAME}.crt"
SITE_CSR="${CERT_DIR}/${CERT_FILENAME}.csr"

mkdir -p "${CERT_DIR}"



if [ ! -f "$ROOT_CA_KEY" ]; then
	printErrorBlock "Development Root CA is missing"
	printInfoLine "Generate new key running command 'docker/lib/makeSelfSignedSSL.sh -r'"
	exit 1
fi

if [ ! -f "$SITE_CRT" ]; then

	openssl genrsa -out ${SITE_KEY} 4096
	openssl req -new -key ${SITE_KEY} -subj "${CERT_SUBJ}" -out ${SITE_CSR}

	echo "authorityKeyIdentifier=keyid,issuer
	basicConstraints=CA:FALSE
	keyUsage = digitalSignature, nonRepudiation, keyEncipherment, dataEncipherment
	subjectAltName = @alt_names

	[alt_names]
	DNS.1 = ${DNS}" > ${SITE_CNF}

	
	openssl x509 -req -passin pass:${PASS} -in ${SITE_CSR} -CA ${ROOT_CA_PEM} -CAkey ${ROOT_CA_KEY} -CAcreateserial -out ${SITE_CRT} -days 825 -sha256 -extfile ${SITE_CNF} -CAserial ${CA_SERIAL}
	
	rm -f ${SITE_CNF} ${SITE_CSR}
fi
