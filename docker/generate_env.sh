	
ifEmpty STACK_ENV_DEFAULT "dev"

ifEmpty STACK_BASE_URL $STACK_BASE_URL_DEFAULT;


if [ "$QUIET" == false ]; then
    read -p "Stack environment [$STACK_ENV_DEFAULT]: " STACK_ENV
fi
ifEmpty STACK_ENV $STACK_ENV_DEFAULT;
saveEnvVar "STACK_ENV"


HTTPS_ENDPOINT_AUTH_URL="https://auth.${STACK_BASE_URL}"
escapeUrl HTTPS_ENDPOINT_AUTH_URL
saveEnvVar "HTTPS_ENDPOINT_AUTH_URL"
unescapeVarString "HTTPS_ENDPOINT_AUTH_URL"


HTTPS_ENDPOINT_APP="https://app.${STACK_BASE_URL}"
escapeUrl HTTPS_ENDPOINT_APP
saveEnvVar "HTTPS_ENDPOINT_APP"
unescapeVarString "HTTPS_ENDPOINT_APP"

if [ "$QUIET" == false ]; then
    read -p "Stack base url [$STACK_BASE_URL_DEFAULT]: " STACK_BASE_URL
fi
ifEmpty STACK_BASE_URL $STACK_BASE_URL_DEFAULT;
saveEnvVar "STACK_BASE_URL"

ifEmpty AUTOREGISTER_HOSTNAME true;
saveEnvVar "AUTOREGISTER_HOSTNAME"


#	Register some global variables if not exists
if [ "$GIT_USERNAME" == "" ]; then
	read -p "Git username [gitlab username]: " GIT_USERNAME
	saveGlobalEnvVar "GIT_USERNAME"
fi

if [ "$GIT_ACCESS_TOKEN" == "" ]; then
	read -p "Git access token [create new at gitlab]: " GIT_ACCESS_TOKEN
	saveGlobalEnvVar "GIT_ACCESS_TOKEN"
fi



# Keep this as true for now, currently seems to work better like this, always.
DOCKER_EXEC_AS_LOCAL_USER=true
saveEnvVar "DOCKER_EXEC_AS_LOCAL_USER"
