FROM mailhog/mailhog as base

ARG STACK_NAME
ARG STACK_VERSION
ARG STACK_ENV

ENV STACK_NAME=${STACK_NAME}
ENV STACK_VERSION=${STACK_VERSION}
ENV STACK_ENV=${STACK_ENV}

LABEL   "fi.oss-solutions.project"="$STACK_NAME" \
        "fi.oss-solutions.version"="$STACK_VERSION" \
        "fi.oss-solutions.env"="$STACK_ENV" \
        "fi.oss-solutions.service"="mailhog"

FROM base as prod


FROM base as dev



