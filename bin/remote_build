#!/bin/bash
source "$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )/../docker/lib/bash_functions.sh"

exitIfEnvironmentIsNotSetUp

importContainerEnvVars "stack"
remoteStackRootDir

if ! ssh_remote_has_access ; then
  printErrorLine "You do not have access to remote target"
  printNoticeLine "run bin/remote_install"
  exit
fi


function getRemoteDetails() {
	REMOTE_GIT_BRANCH=$(ssh_remote "cd $REMOTE_STACK_ROOT_DIR; git rev-parse --abbrev-ref HEAD")
	REMOTE_GIT_ORIGIN=$(ssh_remote "cd $REMOTE_STACK_ROOT_DIR; git rev-parse --abbrev-ref --symbolic-full-name @{u}")
}

function localDetails() {
	GIT_REMOTE_URL=$(git config --get remote.origin.url 2>/dev/null)
	GIT_REMOTE_URL="${GIT_REMOTE_URL:8}"
	GIT_REMOTE_URL="https://$GIT_USERNAME:$GIT_ACCESS_TOKEN@$GIT_REMOTE_URL"
	GIT_BRANCH=$(git rev-parse --abbrev-ref HEAD)
	GIT_ORIGIN=$(git rev-parse --abbrev-ref --symbolic-full-name @{u} 2>/dev/null)
}


exitIfGitHasPendingChanges

wait_remote_to_unlock
set_remote_build_lock

localDetails

EXPECTED_RETURN="origin/${GIT_BRANCH}"
TEST=$(git branch -r 2>&1 |grep $EXPECTED_RETURN | grep -v "HEAD" | xargs)
if [ "$TEST" != "$EXPECTED_RETURN" ]; then
	git push -u origin $GIT_BRANCH >/dev/null 2>&1
	localDetails
else
	git push 2>&1
fi

TEST=$(git push --dry-run 2>&1)
if [ "$TEST" != "Everything up-to-date" ]; then
	printErrorLine "Could not push changes, can not proceed"
	remote_lock_clear
	exit;	
fi


getRemoteDetails

if [ "$GIT_BRANCH" != "$REMOTE_GIT_BRANCH" ]; then
	printInfoLine "Checkout branch $GIT_BRANCH"
	TEST=$(ssh_remote "cd $REMOTE_STACK_ROOT_DIR; git reset --hard >/dev/null 2>&1; git branch --list |grep $GIT_BRANCH")

	if [ "$TEST" != "" ]; then
        ssh_remote_tty "cd $REMOTE_STACK_ROOT_DIR; git checkout $GIT_BRANCH"
    else
        ssh_remote_tty "cd $REMOTE_STACK_ROOT_DIR; git fetch origin >/dev/null 2>&1; git checkout -b $GIT_BRANCH origin/$GIT_BRANCH"
        sleep 2 
    fi  
fi

getRemoteDetails

if [ "$GIT_BRANCH" != "$REMOTE_GIT_BRANCH" ]; then
	printErrorLine "Could not change to correct branch, can not proceed"
	remote_lock_clear
	exit;
fi

if [ "$GIT_ORIGIN" != "$REMOTE_GIT_ORIGIN" ]; then
  printErrorLine "Git origins does not match can not proceed"
  debug "GIT_ORIGIN"
  debug "REMOTE_GIT_ORIGIN"
  remote_lock_clear
  exit;
fi

ssh_remote_tty "cd $REMOTE_STACK_ROOT_DIR; git submodule update; git pull;"
ssh_remote_tty "cd $REMOTE_STACK_ROOT_DIR; bin/build -e $STACK_ENV $@"

remote_lock_clear

say -v "Daniel" "Your remote build has been completed, sir"
